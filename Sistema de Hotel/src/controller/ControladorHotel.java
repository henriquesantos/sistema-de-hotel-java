package controller;

import java.util.List;
import model.hospede.Hospede;
import repository.hospede.CPFJaCadastradoException;
import repository.hospede.HospedeNaoCadastradoException;
import repository.hospede.RepositorioHospede;
import repository.hospede.RepositorioHospedeLista;
import repository.quarto.RepositorioQuarto;
import repository.reserva.RepositorioReserva;

public class ControladorHotel {

    private RepositorioHospede repositorioHospede;
    private RepositorioQuarto repositorioQuarto;
    private RepositorioReserva repositorioReserva;

    public ControladorHotel() throws Exception {
        repositorioHospede = new RepositorioHospedeLista();
    }

    public void inserirHospede(Hospede hospede) throws CPFJaCadastradoException {
        repositorioHospede.InserirHospede(hospede);
    }

    public Hospede buscarHospede(String cpf) throws HospedeNaoCadastradoException {
        return repositorioHospede.buscarHospede(cpf);
    }

    public void excluirHospede(Hospede hospede) throws ControladorException, HospedeNaoCadastradoException {
        if (repositorioReserva.getAll(hospede.getCpf()).isEmpty()) {
            repositorioHospede.DeletarHospede(hospede);
        } else {
            throw new ControladorException("Cliente com contas ativas não pode ser excluído");
        }
    }

    public void exit() {
        // Nada para fazer
    }

    public List<Hospede> getAllClientes() {
        return repositorioHospede.getAll();
    }
}
