
package controller;

import repository.hospede.RepositorioHospede;
import repository.quarto.RepositorioQuarto;
import repository.reserva.RepositorioReserva;


public class ControladorException extends Exception{
    
    public ControladorException(String message){
        super(message);
    } 
}
