package execultar;

import controller.ControladorException;
import controller.ControladorHotel;
import java.util.List;
import java.util.Scanner;
import model.hospede.Hospede;
import repository.hospede.CPFJaCadastradoException;
import repository.hospede.HospedeNaoCadastradoException;

public class Execultar {

    static Scanner scanner = new Scanner(System.in);

    static ControladorHotel controlador;

    public static void main(String[] args) throws Exception {

        controlador = new ControladorHotel();

        insereDadosTeste();

        int opcao;

        do {
            limpaTela();
            System.out.println("MENU PRINCIPAL");
            System.out.println("==== =========");
            System.out.println();
            System.out.println("<1> Cadastro Hospede");
            System.out.println("<2> Cadastro Quarto");
            System.out.println("<3> RESERVA");
            System.out.println("<0> Sair");
            System.out.println();
            System.out.print("Escolha uma opção: ");

            try {
                opcao = Integer.valueOf(scanner.nextLine());
            } catch (Exception e) {
                opcao = 0;
            }

            switch (opcao) {
                case 0:
                    limpaTela();
                    break;
                case 1:
                    cadastroHospedes();
                    break;
                case 2:
                    //cadastroContas();
                    break;
                case 3:
                    //caixaEletronico();
                    break;
            }
        } while (opcao != 0);

        controlador.exit();
        System.out.println("Programa terminado");

    }

    private static void insereDadosTeste() {

        try {

            Hospede hospede01 = new Hospede("1", "João Batista", 'M', "99123-1234");
            controlador.inserirHospede(hospede01);
            Hospede hospede02 = new Hospede("2", "Paula Leite ", 'F', "98765-4321");
            controlador.inserirHospede(hospede02);

//            Conta c1 = new ContaCorrente(cliente01);;
//            controlador.inserirConta(c1);
//            Conta c2 = new ContaCorrente(cliente02);
//            controlador.inserirConta(c2);
        } catch (CPFJaCadastradoException ex) {

        }
    }

    private static void limpaTela() {
        for (int i = 0; i < 25; i++) {
            System.out.println();
        }
    }

    private static void cadastroHospedes() {
        int opcao;
        do {
            limpaTela();
            System.out.println("CADASTRO HOSPEDE");
            System.out.println("======== ========");
            System.out.println();
            System.out.println("<1> Incluir Hospede");
            System.out.println("<2> Alterar Hospede");
            System.out.println("<3> Excluir Hospede");
            System.out.println("<4> Listar Hospedes");
            System.out.println("<0> Menu Principal");
            System.out.println();
            System.out.print("Escolha uma opção: ");

            try {
                opcao = Integer.valueOf(scanner.nextLine());
            } catch (Exception e) {
                opcao = 0;
            }

            switch (opcao) {
                case 0:
                    limpaTela();
                    break;
                case 1:
                    incluirHospede();
                    break;
                case 2:
                    //alterarHospedes();
                    break;
                case 3:
                    excluirHospede();
                    break;
                case 4:
                    listarHospedes();
                    break;
            }
        } while (opcao != 0);
    }

    private static void incluirHospede() {
        limpaTela();
        System.out.println("Cadastro de Hospede");
        System.out.println("===================");
        System.out.print("CPF: ");
        String cpf = scanner.nextLine();
        System.out.print("Nome: ");
        String nome = scanner.nextLine();
        System.out.print("Sexo: ");
        char sexo = scanner.nextLine().charAt(0);
        System.out.print("Telefone: ");
        String fone = scanner.nextLine();

        Hospede hospede = new Hospede(cpf, nome, sexo, fone);

        try {
            controlador.inserirHospede(hospede);
            System.out.println("Hospede cadastrado!");
        } catch (CPFJaCadastradoException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println("tecle <enter> para voltar");
        scanner.nextLine();
    }

    private static void listarHospedes() {
        limpaTela();
        List<Hospede> hospedes = controlador.getAllClientes();
        System.out.printf("CPF          Nome                 Sexo  Telefone\n");
        System.out.printf("============ ==================== ====  ===============\n");
        for (Hospede hospede : hospedes) {
            System.out.printf("%-12s ", hospede.getNome());
            System.out.printf("%12s ", hospede.getCpf());
            System.out.printf("%10s ", String.valueOf(hospede.getSexo()));
            System.out.printf("%15s\n", hospede.getTelefone());
        }

        System.out.println();
        System.out.println("tecle <enter> para voltar");
        scanner.nextLine();
    }
    
     private static void excluirHospede() {
        limpaTela();
        System.out.println("Excluir Hospede");
        System.out.println("==================");
        System.out.print("CPF: ");
        String cpf = scanner.nextLine();

        try {
            Hospede hospede = controlador.buscarHospede(cpf);
            System.out.println();
            System.out.println("Nome: " + hospede.getNome());
            System.out.println("Sexo: " + hospede.getSexo());
            System.out.println("Telefone: " + hospede.getTelefone());
            System.out.println();

            System.out.print("Exclui esse hospede? (s/n)?");
            String resposta = scanner.nextLine();

            if (resposta.equalsIgnoreCase("s")) {
                controlador.excluirHospede(hospede);
                System.out.println("Hospede excluído!");
            }

        } catch (HospedeNaoCadastradoException | ControladorException ex) {
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("tecle <enter> para voltar");
        scanner.nextLine();
    }

}
