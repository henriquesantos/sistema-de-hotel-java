
package repository.quarto;

import model.quarto.Quarto;
import java.util.List;

public interface RepositorioQuarto {
    
    Quarto inserirQuarto(Quarto quarto) throws QuartoJaCadastradoException;

    void alterarQuarto(Quarto quarto) throws QuartoNaoCadastradoException;

    void deletarQuarto(Quarto quarto) throws QuartoNaoCadastradoException;

    Quarto buscarQuarto(String numero) throws QuartoNaoCadastradoException;
    
     List<Quarto> getAll();  
    
}
