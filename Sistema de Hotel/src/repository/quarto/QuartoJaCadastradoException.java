
package repository.quarto;


public class QuartoJaCadastradoException extends Exception {
    
    public QuartoJaCadastradoException(){
        super("Quarto já cadastro!");
    }
    
}
