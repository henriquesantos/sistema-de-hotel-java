
package repository.quarto;


public class QuartoNaoCadastradoException extends Exception {
    
    public QuartoNaoCadastradoException(){
        super("Quarto não cadastrado!");
    }
    
}
