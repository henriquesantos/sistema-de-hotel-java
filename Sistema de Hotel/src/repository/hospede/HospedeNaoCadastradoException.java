
package repository.hospede;

public class HospedeNaoCadastradoException extends Exception {
    
    public HospedeNaoCadastradoException(){
        super("Hospede não Cadastrado!");
    }
    
   
}
