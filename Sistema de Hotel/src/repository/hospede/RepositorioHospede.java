package repository.hospede;

import model.hospede.Hospede;
import java.util.List;

public interface RepositorioHospede {

    void InserirHospede(Hospede hospede) throws CPFJaCadastradoException;

    void AlterarHospede(Hospede hospede) throws HospedeNaoCadastradoException;

    void DeletarHospede(Hospede hospede) throws HospedeNaoCadastradoException;

    Hospede buscarHospede(String CPF) throws HospedeNaoCadastradoException;

    List<Hospede> getAll();
}
