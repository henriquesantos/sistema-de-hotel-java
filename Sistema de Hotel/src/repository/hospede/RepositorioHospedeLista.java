package repository.hospede;

import java.util.ArrayList;
import java.util.List;
import model.hospede.Hospede;

public class RepositorioHospedeLista implements RepositorioHospede {

    List<Hospede> hospedes;

    public RepositorioHospedeLista() {
        hospedes = new ArrayList<>();
    }

    @Override
    public void InserirHospede(Hospede hospede) throws CPFJaCadastradoException {
        try {
            buscarHospede(hospede.getCpf());
            throw new CPFJaCadastradoException();
        } catch (HospedeNaoCadastradoException ex) {
            hospedes.add(hospede);
        }
    }

    @Override
    public void AlterarHospede(Hospede hospede) throws HospedeNaoCadastradoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DeletarHospede(Hospede hospede) throws HospedeNaoCadastradoException {
        if (!hospedes.remove(hospede)) {
            throw new HospedeNaoCadastradoException();
        }
    }

    @Override
    public Hospede buscarHospede(String CPF) throws HospedeNaoCadastradoException {
        for (Hospede hospede : hospedes) {
            if (hospede.getCpf().equals(CPF)) {
                return hospede;
            }
        }
        throw new HospedeNaoCadastradoException();
    }

    @Override
    public List<Hospede> getAll() {
        return new ArrayList<>(hospedes);
    }

}
