
package repository.reserva;


public class ReservaNaoCadastradaException extends Exception {
    
    public ReservaNaoCadastradaException(){
        super("Reserva não cadastrada");
    }
    
}
