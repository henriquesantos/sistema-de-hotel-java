
package repository.reserva;

import model.reserva.Reserva;
import java.util.List;
import java.util.Date;

public interface RepositorioReserva {
    
    Reserva inserirReserva(Reserva reserva) throws ReservaJaCadastradaException;

    void alterarReserva(Reserva reserva) throws ReservaNaoCadastradaException;

    void deletarReserva(Reserva reserva) throws ReservaNaoCadastradaException;

    Reserva buscarReserva(Date data) throws ReservaNaoCadastradaException; 
    
    List<Reserva> getAll();
    
    List<Reserva> getAll(String cpf);
    
}
