
package repository.reserva;

public class ReservaJaCadastradaException extends Exception {
    
    public ReservaJaCadastradaException(){
        super("Reserva já cadastrada");
    }
    
}
