package model.hospede;

import java.io.Serializable;

public class Hospede implements Serializable {

    private String nome;
    private String cpf;
    private char sexo;
    private String telefone;
  
    
    public Hospede(String nome, String cpf, char sexo, String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.sexo = sexo;
        this.telefone = telefone;
    }
    
    
    public void setNome(String nome) {
        this.nome = nome;
    } 
    
    public String getNome() {
        return nome;
    }

     public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public String getCpf() {
        return cpf;
    }
    
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    
    public char getSexo() {
        return sexo;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public String getTelefone() {
        return telefone;
    }
}
