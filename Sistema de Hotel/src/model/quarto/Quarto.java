package model.quarto;

import java.io.Serializable;

public class Quarto implements Serializable {
    
    private String numero;
    private String descricao;
    private double preco;
    
    public Quarto(String numero, String descricao, double preco) {
        this.numero = numero;
        this.descricao = descricao;
        this.preco = preco;
    }


    public void setNumero(String numero) {
        this.numero = numero;
    }
    
     public String getNumero() {
        return numero;
    }
    

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
     public String getDescricao() {
        return descricao;
    }

    public void setPreco(double preco){
        this.preco = preco;
    }
    
    public double getPreco(){
        return preco;
    }
    
}
